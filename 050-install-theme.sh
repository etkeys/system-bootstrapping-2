#!/bin/bash

set -e

if [[ $EUID -eq 0 ]]; then
    printf "This script cannot be run as root!"
    exit 1
fi

cd $HOME
ls .themes/*.tar.gz | head -n 1 | xargs tar -vxf
