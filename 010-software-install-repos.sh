#!/bin/bash

if [[ $EUID -ne 0 ]]; then
    printf "This script must be run as root!"
    exit 1
fi

# Microsoft .NET Core package singing key and repository info
wget -q "https://packages.microsoft.com/config/ubuntu/$(lsb_release --short --release)/packages-microsoft-prod.deb"
dpkg -i packages-microsoft-prod.deb

# Regolith Desktop
yes | add-apt-repository ppa:regolith-linux/release

# Finish up
apt-get update
