#!/usr/bin/env python3
import argparse
import collections
import csv
import itertools
import logging
from string import Template
import subprocess
import time

__ARG_VAR_LIST_TAGS__ = 'list-tags'
__ARG_VAR_LIST_TAGS_ALT__ = __ARG_VAR_LIST_TAGS__.replace('-','_')
__ARG_VAR_SIMULATE__ = 'simulate'
__ARG_VAR_TAGS__ = 'tags'

__EXIT_INVALID_USER__ = 2
__EXIT_USER_REQUEST__ = 3

__INSTALL_CMD_APT__ = Template('apt-get install -y $package $additional_commands')
__INSTALL_CMD_PIP__ = Template('yes | pip install -q $package')
__INSTALL_CMD_SNAP__ = Template('snap install $package $additional_commands')

logger = logging.getLogger(__name__)

def check_euid():
    from os import geteuid
    euid = geteuid()
    if euid > 0:
        logger.error("Not running as root user, aborting.")
        exit(__EXIT_INVALID_USER__)
        

def get_csv_data(file):
    data = [r for r in csv.DictReader(file)]
    for row in data:
        raw_tags = row['tags']
        if raw_tags == '':
            row['tags'] = ['default']
        else:
            row['tags'] = raw_tags.split(';')

    return data

def filter_data_to_requested_tags(data, request_tags):
    def is_package_tag_requested(data_item):
        intersect = [value for value in data_item['tags'] if value in request_tags]
        return not (intersect == [])

    result = filter(is_package_tag_requested, data)
    return result

def compute_elapsed_time(start):
    end = time.perf_counter()
    hours, rem = divmod(end-start, 3600)
    minutes, seconds = divmod(rem, 60)
    return f'{int(hours):0>2}:{int(minutes):0>2}:{int(seconds):0>2}' 

def perform_install(data, args):
    start_time = time.perf_counter()
    orig_data_count = len(data)
    data = list(filter_data_to_requested_tags(data, args[__ARG_VAR_TAGS__]))
    try_data_count = len(data)

    print(f'Installing {try_data_count} of {orig_data_count} packages with tags {args[__ARG_VAR_TAGS__]}...')
    for num, item in enumerate(data, start=1):
        provider = item['provider']
        cmd = ''
        if provider == 'apt':
            cmd = __INSTALL_CMD_APT__.substitute(item)
        elif provider == 'pip':
            cmd = __INSTALL_CMD_PIP__.substitute(item)
        elif provider == 'snap':
            cmd = __INSTALL_CMD_SNAP__.substitute(item)
        
        elapsed_time = compute_elapsed_time(start_time)
        print(f'[{elapsed_time}]({num}/{try_data_count}) {item["package"]}')        
        if args[__ARG_VAR_SIMULATE__]:
            print(cmd)
            time.sleep(1.2)
        else:
            result = subprocess.run(
                [cmd],
                check=True,
                encoding='utf-8',
                shell=True,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
            )


def main(args):
    if not args[__ARG_VAR_SIMULATE__]:
        check_euid()

    data = get_csv_data(args['file'])
    if args[__ARG_VAR_LIST_TAGS_ALT__]:
        data = sorted(
            set(
                itertools.chain.from_iterable(
                    map(lambda x: x['tags'], data))))
        for item in data:
            print(item)

    else:
        if args[__ARG_VAR_TAGS__] is None:
            logger.warning("No tags were passed. This will cause only packages tagged with 'default' to be installed.")
            resp = input("Continue installing only 'default' packages (y/n)? ")

            if resp == '' or resp[0].lower() == 'n':
                print("Response is empty or 'n' given, exiting.")
                exit(__EXIT_USER_REQUEST__)

            args[__ARG_VAR_TAGS__] = ['default']
        
        perform_install(data, args)


    

if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.set_defaults(allow_abbrev=False)

    # positionals
    arg_parser.add_argument(
        'file',
        type=argparse.FileType('r'),
        help='Path to a csv file that contains list of packages to install')

    # optionals
    arg_parser.add_argument(
        f'--{__ARG_VAR_LIST_TAGS__}',
        action='store_true',
        help='Only show what tags are present in the provided list file')

    arg_parser.add_argument(
        f'--{__ARG_VAR_SIMULATE__}',
        action='store_true',
        help='Print what would be done, but don\'t actually do anything'
    )

    arg_parser.add_argument(
        '-t',f'--{__ARG_VAR_TAGS__}',
        dest=f'{__ARG_VAR_TAGS__}',
        nargs='+',
        help='Tag of packages to install. Must be the last argument.')
    
    args = arg_parser.parse_args()
    main(vars(args))
    
