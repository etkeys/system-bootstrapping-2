#!/bin/bash

set -e

if [[ $EUID -eq 0 ]]; then
    printf "This script cannot be run as root!"
    exit 1
fi

cd $HOME

yadm clone "https://gitlab.com/etkeys/dotfiles.git"
yadm stash drop
yadm decrypt
yadm remote set-url --push origin "git@gitlab.com:etkeys/dotfiles.git"

cd $HOME

printf '\nYou should log out and then log back in.'
