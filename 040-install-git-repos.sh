#!/bin/bash

set -e

if [[ $EUID -eq 0 ]]; then
    printf "This script cannot be run as root!"
    exit 1
fi

cd $HOME

mkdir bin
mkdir repos

cd repos

# my personal scripts, path should already be in $PATH
git clone git@gitlab.com:etkeys/scripts.git

# my i3blocks blocklets
git clone git@gitlab.com:etkeys/i3blocks-blocklets.git
cd i3blocks-blocklets
mkdir -p $HOME/bin/i3blocks
./deploy
cd ..

# Quickemu script, maintained by Martin Wimpress
if [ -d /snap/qemu-virgil ]; then
    git clone https://github.com/wimpysworld/quickemu.git
    ln -s $HOME/repos/quickemu/quickemu $HOME/bin/quickemu 
fi

cd $HOME

printf "\nNOTE: For best results, it is best to log out and then back in."