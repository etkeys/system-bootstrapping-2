# Things that have to be done manually

It's not that I'm lazy, but I had two machines I needed to setup and I 
spent enough time banging my head against a wall trying to figure things
out. For the things here, I just didn't do a good job fitting them for
automation. I'll fix this later, promise!


## Theme

### Look and feel

Need to figure out how to tell tar to extract to a particular directory.
-C "$tdir" doesn't work, it just prints the files but I don't know where 
they go ...

```bash
tdir=$(mktemp -d)
cp $HOME/.themes/*.tar.gz "$tdir"
# this extracts from tmp into home dir... how to specify rando dir?
# tar -C "$tdir" -xf doesn't seem to work
ls $tdir/*.tar.gz | head -n 1 | xargs tar -vxf 
rm $tdir/*.tar.gz
cp -rfT $tdir/* $HOME/
```

### Sounds

**AS ROOT**

cd /usr/share/sounds/ubuntu/stereo
/home/erik/repos/scripts/cpb dialog-{error,warning}.ogg
ln -sfn dialog-error.ogg /home/erik/Public/sounds/newUbuntu/dialog-error.ogg
ln -sfn dialog-warning.ogg /home/erik/Public/sounds/newUbuntu/dialog-warning.ogg

## Handbrake

**AS ROOT**

```bash
apt install libdvd-pkg
dpkg-reconfigure libdvd-pkg
```

## Cron

```bash
crontab $XDG_CONFIG_HOME/cron.d/crontab.tab
```

## Firefox extensions

- Google App Launcher
- LastPass
- Text Contrast for Dark Themes
- Tree Style Tab
- uBlock Origin
- Vim Vixen


