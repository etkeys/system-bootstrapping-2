#!/bin/bash

if [[ $EUID -eq 0 ]]; then
    echo "This script should not be run as root."
    exit 1
fi

curl -s "https://gitlab.com/etkeys/scripts/-/raw/master/update-vim-plugins"
