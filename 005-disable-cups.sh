#!/bin/bash

# CUPS is a printing interface service. This script will disable this service
# because it could hang when shutting down. Only use this script when cups is on
# the system but you don't need it.

if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root!"
    exit 1
fi

systemctl stop cups
systemctl stop cups-browsed
systemctl disable cups
systemctl disable cups-browsed

