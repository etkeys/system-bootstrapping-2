#!/bin/bash

if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root!"
    exit 1
fi

$datestamp=$(date +%y%m%d%H%M)
cp /etc/default/grub /etc/default/grub.$datestamp.bak

sed -i -E "
    s/^GRUB_TIMEOUT_STYLE=.+/GRUB_TIMEOUT_STYLE=menu/;
    s/^GRUB_TIMEOUT=.+/GRUB_TIMEOUT=5/;
    s/^GRUB_CMDLINE_LINUX_DEFAULT.+/GRUB_CMDLINE_LINUX_DEFAULT=\"\"/
" /etc/default/grub

update-grub

